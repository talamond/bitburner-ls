# Bitburner LS Reimagined

A simple tool to extend the hardcoded ls command in Bitburner to have a little 
bit of additional functionality.

At the moment, the biggest feature added is just hidden files, following the 
format of Linux/Unix systems (any file or folder name starting with '.' is
hidden). Additionally, for a clean user experience this hides non-user files
in the same fashion (.exe, .lit, .msg, etc).

If you want to see hidden files, just pass `-a` and it will show hidden files
again. Alternatively you can pass `-l` for an alternative list view format.

### Installation

The recommended install method is via [bitpacker](https://github.com/davidsiems/bitpacker/tree/main).

After installing bitpacker just run `bp add ls` to install (including adding
the suggested aliases). And afterwards run `bp browse` to see what else is
available (the rm re-implementation someone else did is a good compliment).

If you want to install it raw, this only requires the ls.js file given here,
and you can replace the built in ls with `alias ls=run ls.js`.

### Noted Includes

This tool relies on another of my projects, the [Bitburner Terminal Library](https://gitlab.com/talamond/bitburner-terminal-library).

It dynamically imports the library via the Statically CDN so you do not have to
install the library separately.